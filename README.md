# Asqatasun

[![License : AGPL v3](https://img.shields.io/badge/license-AGPL3-blue.svg)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/LICENSE)
[![Build Status](https://gitlab.com/asqatasun/Asqatasun/badges/master/pipeline.svg)](https://gitlab.com/asqatasun/Asqatasun/pipelines?scope=branches)
[![Contributing welcome](https://img.shields.io/badge/contributing-welcome-brightgreen.svg?style=flat-square)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CONTRIBUTING.md)
[![Code of Conduct](https://img.shields.io/badge/code%20of-conduct-ff69b4.svg?style=flat-square)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CODE_OF_CONDUCT.md)

![Asqatasun Logo](http://forum.asqatasun.org/uploads/default/original/1X/e16a2b9b7f5a4dc756f03630923290c695c762c9.png)

Asqatasun is an opensource website analyzer, used for web **accessibility** (a11y).

## Demo

[https://app.asqatasun.org](https://app.asqatasun.org)

[![https://app.asqatasun.org](https://gitlab.com/asqatasun/Asqatasun/-/raw/master/documentation/en/Images/app.asqatasun.org_FR_690x340.png)](https://app.asqatasun.org)

## Features

**web accessibility assessment** `#a11y` (French RGAA 4, based on WCAG)

- scan a single page
- scan a whole site (crawler)
- scan offline file (e.g. template being created but not online yet)
- scan a user-workflow like site registration, form completion or e-commerce checkout with **Asqatasun scenarios**.

What tests are covered:

- all the "tag and attributes tests" like missing alt, table headers check, frame title...
- color contrast
- language specification
- downloadable files / office files (spreadsheet, word-processor...)
- switch of context
- ...

## Vision

1. Automate as much as we can and even more :)
2. Be 200% reliable (don't give erroneous result)
3. have technological fun

## Installation and documentation

- [**Documentation** website](https://doc.asqatasun.org/v5/en/)
- [**Vagrant** repository](https://gitlab.com/asqatasun/asqatasun-vagrant)
- [**Docker** repository](https://gitlab.com/asqatasun/asqatasun-docker)

## Contact and discussions

- [Asqatasun forum](https://forum.asqatasun.org/)
- [Element.io: +asqatasun:matrix.org](https://app.element.io/#/group/+asqatasun:matrix.org)
- email to `asqatasun AT asqatasun dot org` (only English, French and Klingon is spoken :) )

## Contribute

We would be glad to have you on board! You can help in many ways:

1. Use Asqatasun on your sites !
1. Give us [feedback on the forum](https://forum.asqatasun.org)
1. [Fill in bug report](https://gitlab.com/asqatasun/Asqatasun/-/issues)
1. [Contribute](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CONTRIBUTING.md) code

## License

[AGPL v3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/LICENSE)

## Have Fun

Happy testing !

[Asqatasun Team](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/documentation/en/asqatasun-team.md)
